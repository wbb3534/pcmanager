package com.seop.pcmanager.service;

import com.seop.pcmanager.entity.Manager;
import com.seop.pcmanager.repository.ManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagerService {
    private final ManagerRepository managerRepository;

    public void setManager(String pcName, String contactName) {
        Manager addData = new Manager();
        addData.setPcName(pcName);
        addData.setContactName(contactName);

        managerRepository.save(addData);
    }
}
