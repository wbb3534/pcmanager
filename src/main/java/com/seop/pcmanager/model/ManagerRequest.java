package com.seop.pcmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManagerRequest {
    private String pcName;
    private String contactName;
}
