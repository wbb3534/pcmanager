package com.seop.pcmanager.controller;

import com.seop.pcmanager.model.ManagerRequest;
import com.seop.pcmanager.service.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manager")
@RequiredArgsConstructor
public class ManagerController {
    private final ManagerService managerService;

    @PostMapping("/data")
    public String getData(@RequestBody ManagerRequest request) {
        managerService.setManager(request.getPcName(), request.getContactName());
        return "전송 됨";
    }
}
